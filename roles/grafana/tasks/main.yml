---

- name: Install dependencies
  ansible.builtin.apt:
    name: "{{ grafana_dependencies }}"
    state: present
    update_cache: true
  register: install_dependencies
  until: install_dependencies is succeeded

- name: Download grafana deb package
  ansible.builtin.get_url:
    url: "{{ grafana_package_url }}"
    dest: "/tmp/grafana_{{ grafana_version }}_amd64.deb"
    checksum: "sha256:{{ grafana_package_checksum }}"
    owner: root
    group: root
    mode: 0775
  register: _package_grafana
  until: _package_grafana is succeeded
  retries: 5
  delay: 2

- name: Install grafana deb package
  ansible.builtin.apt:
    deb: "{{ _package_grafana.dest }}"
    state: present
    update_cache: true
  notify:
    - Start grafana

# - name: Ensure grafana configuration directories exist
#   ansible.builtin.file:
#     path: "{{ item }}"
#     state: directory
#     owner: root
#     group: grafana
#     mode: 0755
#   loop:
#     - "/etc/grafana"
#     - "/etc/grafana/provisioning"
#     - "/etc/grafana/provisioning/datasources"
#     - "/etc/grafana/provisioning/dashboards"
#     - "/etc/grafana/provisioning/notifiers"
#     - "/etc/grafana/provisioning/plugins"

- name: Create grafana main configuration file
  ansible.builtin.template:
    src: grafana.ini.j2
    dest: /etc/grafana/grafana.ini
    owner: root
    group: grafana
    mode: 0640
  no_log: true
  notify: Restart grafana

- name: Create grafana directories
  ansible.builtin.file:
    path: "{{ item }}"
    state: directory
    mode: 0755
    owner: grafana
    group: grafana
  loop:
    - "{{ grafana_logs_dir }}"
    - "{{ grafana_data_dir }}"
    - "{{ grafana_data_dir }}/dashboards"
    - "{{ grafana_data_dir }}/plugins"

- name: Flush handlers before configuring datasources and dashboards
  ansible.builtin.meta: flush_handlers

- name: Wait for grafana to start (http/s)
  ansible.builtin.wait_for:
    host: "{{ grafana_address }}"
    port: "{{ grafana_port }}"
  when: grafana_server.protocol is undefined or grafana_server.protocol in ['http', 'https']

- name: Wait for grafana to start (socket)
  ansible.builtin.wait_for:
    path: "{{ grafana_server.socket }}"
  when: grafana_server.protocol is defined and grafana_server.protocol == 'socket'

- name: Add grafana datasources
  ansible.builtin.include_tasks: datasources.yml
  when: grafana_datasources != []

- name: Add grafana notifications
  ansible.builtin.include_tasks: notifications.yml
  when: grafana_alert_notifications | length > 0

- name: "Check if there are any dashboards in local {{ grafana_dashboards_dir }}"
  become: false
  ansible.builtin.set_fact:
    found_dashboards: "{{ lookup('fileglob', grafana_dashboards_dir + '/*.json', wantlist=True) }}"

- name: Add grafana dashboards
  ansible.builtin.include_tasks: dashboards.yml
  when: grafana_dashboards | length > 0 or found_dashboards | length > 0
