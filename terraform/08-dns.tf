resource "yandex_dns_zone" "zone1" {
  name        = "my-public-zone"
  description = "Test public zone"

  labels = {
    label1 = "deusopsclass.domain"
  }

  zone   = "${var.domain-name}."
  public = true
}

resource "yandex_dns_recordset" "web" {
  count   = var.web-instances
  zone_id = yandex_dns_zone.zone1.id
  name    = "${var.domain-name}."
  type    = "A"
  ttl     = 200
  data    = [yandex_compute_instance.web[count.index].network_interface.0.nat_ip_address]
}

resource "yandex_dns_recordset" "monitoring" {
  zone_id = yandex_dns_zone.zone1.id
  name    = "monitoring.${var.domain-name}."
  type    = "A"
  ttl     = 200
  data    = [yandex_compute_instance.monitoring.network_interface.0.nat_ip_address]
}
