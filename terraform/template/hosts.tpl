[web_srv]
%{ for index, ip in web_external_ip ~}
${web_host}-${index + 1} ansible_host=${ip} ansible_user=${web_user} ansible_ssh_private_key_file=${web_ssh}
%{ endfor ~}

[monitor_srv]
${monitoring_host} ansible_host=${monitoring_external_ip} ansible_user=${monitoring_user} ansible_ssh_private_key_file=${monitoring_ssh}

[all:children]
web_srv
monitor_srv

[all:vars]
%{ for index, ip in web_inner_ip ~}
web${index + 1}_in_ip = ${ip}
%{ endfor ~}
monitoring_in_ip = ${monitoring_inner_ip}
d_name = ${domain_name}
