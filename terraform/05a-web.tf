data "template_file" "web_user" {
  template = file(var.instance-user-template)
  vars = {
    ssh_user  = var.web-ssh-user
    ssh_key   = file(join("", [var.web-ssh-key-path, ".pub"]))
  }
}

resource "yandex_compute_instance" "web" {
  count           = var.web-instances
  name            = "${var.web-instance-name}-${count.index + 1}"
  hostname        = "${var.web-instance-name}-${count.index + 1}"
  description     = "web app server"

  platform_id               = var.instance-platform-id
  allow_stopping_for_update = true

  resources {
    cores         = var.instance-cores
    memory        = var.instance-memory
    core_fraction = var.instance-core-fraction
  }

  boot_disk {
    initialize_params {
      image_id = var.instance-image-id
      type     = var.instance-disk-type
      size     = var.instance-disk-size
    }
  }

  network_interface {
    subnet_id = yandex_vpc_subnet.subnet_terraform.id
    nat       = true
  }

  scheduling_policy {
    preemptible = var.is-auto-shutdown
  }

  metadata = {
    user-data = data.template_file.web_user.rendered
  }
}

