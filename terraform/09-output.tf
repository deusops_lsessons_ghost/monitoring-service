output "internal_ip_web_server" {
  value = yandex_compute_instance.web.*.network_interface.0.ip_address
}
output "internal_ip_monitoring_server" {
  value = yandex_compute_instance.monitoring.network_interface.0.ip_address
}
output "external_ip_web_server" {
  value = yandex_compute_instance.web.*.network_interface.0.nat_ip_address
}
output "external_ip_monitoring_server" {
  value = yandex_compute_instance.monitoring.network_interface.0.nat_ip_address
}


# TODO Разобраться как уложить все в цикл или хотя бы последние два блока .
resource "local_file" "ansible_hosts" {
  content = templatefile("./template/hosts.tpl",
   {
    domain_name     = var.domain-name,

    web_user        = var.web-ssh-user,
    web_ssh         = var.web-ssh-key-path,
    web_host        = var.web-instance-name,
    web_inner_ip    = "${yandex_compute_instance.web.*.network_interface.0.ip_address}",
    web_external_ip = "${yandex_compute_instance.web.*.network_interface.0.nat_ip_address}"

    monitoring_user        = var.monitoring-ssh-user,
    monitoring_ssh         = var.monitoring-ssh-key-path,
    monitoring_host        = var.monitoring-instance-name,
    monitoring_inner_ip    = "${yandex_compute_instance.monitoring.network_interface.0.ip_address}",
    monitoring_external_ip = "${yandex_compute_instance.monitoring.network_interface.0.nat_ip_address}"
  }
)
filename = "../hosts"
}
